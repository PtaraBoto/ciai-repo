package booking;


import java.util.ArrayList;
import java.util.Collection;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.transaction.annotation.Transactional;

import booking.dataBaseUtil.PopulateDB;
import booking.model.Booking;
import booking.model.Hotel;
import booking.model.HotelComment;
import booking.model.Role;
import booking.model.Room;
import booking.model.RoomType;
import booking.model.User;
import booking.repository.BookingRepository;
import booking.repository.HotelCommentRepository;
import booking.repository.HotelRepository;
import booking.repository.RoleRepository;
import booking.repository.RoomRepository;
import booking.repository.RoomTypeRepository;
import booking.repository.UserRepository;


@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled=true)
public class Application implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	private static final String prompt = "-------->";

	@Autowired
	HotelRepository hotels;

	@Autowired
	RoomTypeRepository roomtypes;

	@Autowired
	RoomRepository rooms;

	@Autowired
	HotelCommentRepository hotelcommnets;

	@Autowired
	UserRepository users;

	@Autowired
	RoleRepository roles;
	
	@Autowired
	BookingRepository bookings;

	/**
	 * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application.
	 * The run() method returns an ApplicationContext where all the beans that were created 
	 * either by your app or automatically added thanks to Spring Boot are.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	@Transactional
	public void run(String... strings){

		settingUpData();

	}

	private void settingUpData(){
		log.info(prompt + "Setting up seed data");

		hotels.deleteAll();
		roomtypes.deleteAll();
		rooms.deleteAll();
		hotelcommnets.deleteAll();
		users.deleteAll();
		roles.deleteAll();
		bookings.deleteAll();
		
		PopulateDB popdb = new PopulateDB();

		//seed Roles
		ArrayList<Role> seedsRoles = popdb.seedRoles();
		roles.save(seedsRoles);
		log.info(prompt + "Roles saved");

		//seed Users
		ArrayList<User> seedsUser = popdb.seedUsers(seedsRoles);
		users.save(seedsUser);
		log.info(prompt + "Users saved");
		
		// seed Hotels
		Collection<Hotel> seedHotel = popdb.seedsHotel();
		hotels.save(seedHotel);
		log.info(prompt + "Hotels saved");

		//seed RoomTypes
		Collection<RoomType> seedRoomTypes = popdb.seedsRoomType(seedHotel);
		roomtypes.save(seedRoomTypes);
		log.info(prompt + "RoomTypes saved");

		//seed Rooms
		Collection<Room> seedsRooms = popdb.seedRooms(seedRoomTypes);
		rooms.save(seedsRooms);
		log.info(prompt + "Rooms saved");

		//seed comments
		Collection<HotelComment> seedsHotelComment = popdb.seedsHotelComments(seedHotel, seedsUser);
		hotelcommnets.save(seedsHotelComment);
		log.info(prompt + "Comments saved");
		
		//seed booking
		Collection<Booking> seedsBooking = popdb.seedBookings(seedRoomTypes);
		bookings.save(seedsBooking);
		log.info(prompt + "Bookings saved");
	}
}


