package booking.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COMMENT")
public class HotelComment {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String commenttext;
	private Date date;
	private boolean status;
	
	@ManyToOne
	private User user;

	@ManyToOne
	private Hotel hotel;

	public HotelComment(){}
	
	public HotelComment(User user, Hotel hotel){
		this.hotel = hotel;
		this.user = user;
		date = new Date();
		status = false;
	}
	
	public HotelComment(User user, String comment, Date date, Hotel hotel, boolean status){
		this.user = user;
		this.commenttext = comment;
		this.date = date;
		this.hotel = hotel;
		this.status = status;
	}

    //<----------- HotelComment set and get variables ----------->
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCommenttext() {
		return commenttext;
	}

	public void setCommenttext(String comment) {
		this.commenttext = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	//<----------- HotelComment set and get Collections ----------->
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	@Override
    public String toString() {
    	return "User: " + user.getName() + " Comment: " + getCommenttext() + " date: " + date.toString();
    }
}
