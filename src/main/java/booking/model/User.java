package booking.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User {
	
	@GeneratedValue
	@Id
    @Column(name="USER_ID")
	private Long id;
	@Column(unique = true)
	private String name;
	private String password;
	private int status;
	
	@ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "USER_ROLES", 
//             joinColumns = { @JoinColumn(name = "USER_ID") }, 
//             inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") })
    private Collection<Role> roles = new ArrayList<Role>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy="bookingUser")
    private Collection<Booking> bookings = new ArrayList<Booking>();
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="user")
    private Collection<HotelComment> comments = new ArrayList<HotelComment>();
	
	public User() {
		status = 0;
	}
	
	public User(String username, String password, Collection<Role> roles, int status) {
		this.name = username;
		this.password = password;
		this.roles = roles;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Collection<HotelComment> getComments() {
		return comments;
	}

	public void setComments(Collection<HotelComment> comments) {
		this.comments = comments;
	}

	public Collection<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(Collection<Booking> bookings) {
		this.bookings = bookings;
	}

	@Override
    public String toString() {
    	return "Id: " + getId() + ", Name: " + getName();
    	
    }
}