package booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "ROOM")
public class Room {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private Integer floor;
    private String name;   

    @ManyToOne
    private RoomType roomtype;
   
    public Room() {}
    
    public Room(RoomType roomtype) {
    	this.roomtype = roomtype;
    }
    
    public Room(Integer floor, String name, RoomType roomtype) {
    	this.floor = floor;
    	this.name = name;

    	this.roomtype = roomtype; 
    	roomtype.getRooms().add(this);
    }

    //<----------- Room set and get variables ----------->
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}
    
	
    //<----------- room set and get dependecies ----------->

	public RoomType getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(RoomType roomtype) {
		this.roomtype = roomtype;
	}

	//<----------- ToString ----------->
    @Override
    public String toString() {
    	return "Id: " + getId() + " Name: " + getName() ;
    }
}

