package booking.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROOM_TYPE")
public class RoomType {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	private String name;
    private String description; 
    private float price;

    @ManyToOne
    private Hotel hotel;
    
	@OneToMany(cascade = CascadeType.ALL, mappedBy="roomtype")
    private Collection<Room> rooms = new ArrayList<>();
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="bookedRoomType")
    private Collection<Booking> bookings = new ArrayList<>();
 
    public RoomType() {}
    
    public RoomType(Hotel hotel) {
    	this.hotel = hotel;
    }
    
    public RoomType(String name, String description, float price, Hotel hotel) {
    	this.description = description;
    	this.name = name;
    	this.price = price;
    	
    	this.hotel = hotel; 
    	hotel.getRoomTypes().add(this);
    }

    //<----------- Hotel set and get variables ----------->
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

    //<----------- Hotel set and get Collections ----------->
	public Collection<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Collection<Room> rooms) {
		this.rooms = rooms;
	}
	
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Collection<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(Collection<Booking> bookings) {
		this.bookings = bookings;
	}

	//<----------- ToString ----------->
	@Override
    public String toString() {
    	return "Id: " + getId() + "\nName: " + getDescription();
    }

}

