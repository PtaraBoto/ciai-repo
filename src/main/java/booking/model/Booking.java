package booking.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BOOKING")
public class Booking {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private float finalPrice;
	private int accepted;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date endDate;
	
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
	private RoomType bookedRoomType;
	
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
	private User bookingUser;
	
	public Booking(){
		
	}
	
	public Booking(float finalPrice, Date startDate, Date endDate, RoomType bookedRoomType, User bookingUser, int accepted){
		this.finalPrice = finalPrice;
		this.startDate = startDate;
		this.endDate = endDate;
		this.bookedRoomType = bookedRoomType;
		this.bookingUser = bookingUser;
		this.accepted = accepted;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(float finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getAccepted() {
		return accepted;
	}

	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}

	public RoomType getBookedRoomType() {
		return bookedRoomType;
	}

	public void setBookedRoomType(RoomType bookedRoomType) {
		this.bookedRoomType = bookedRoomType;
	}

	public User getBookingUser() {
		return bookingUser;
	}

	public void setBookingUser(User bookingUser) {
		this.bookingUser = bookingUser;
	}
}
