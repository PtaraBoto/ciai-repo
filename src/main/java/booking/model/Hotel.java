package booking.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "HOTEL")
public class Hotel {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    private String name; 

	private String address;
    private String category;
    private Integer rating;
    
    @ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
    private User manager;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy="hotel")
    private Collection<RoomType> roomTypes = new ArrayList<RoomType>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy="hotel")
    private Collection<HotelComment> hotelcomments = new ArrayList<HotelComment>();
    
    public Hotel() {}
    
    public Hotel(String name, String address, String category, int rating, User manager) {
    	this.name = name;
    	this.address = address;
    	this.category = category;
    	this.rating = rating;
    	this.manager = manager;
    }

    //<----------- Hotel set and get variables ----------->
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    public Integer getRating() {
        return rating;
    }

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public User getManager() {
		return manager;
	}

	public void setManager(User manager) {
		this.manager = manager;
	}
    
    //<----------- Hotel set and get Collections ----------->
    public Collection<RoomType> getRoomTypes() {
		return roomTypes;
	}

	public void setRoomTypes(Collection<RoomType> rooms) {
		this.roomTypes = rooms;
	}
	
	
	public Collection<HotelComment> getHotelcomments() {
		return hotelcomments;
	}

	public void setHotelcomments(Collection<HotelComment> hotelcomments) {
		this.hotelcomments = hotelcomments;
	}

	//<----------- ToString ----------->
    @Override
    public String toString() {
    	return "Id: " + getId() + ", Name: " + getName()
    	+ ", Address: " + getAddress() + ", Category: " + getCategory()
    	+ ", Rating: " + getRating();
    }
}
