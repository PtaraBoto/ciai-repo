package booking.configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;


@Component
public class DateFormatter implements Formatter<Date> {
	
	@Override
	public String print(Date arg0, Locale arg1) {
		// TODO Auto-generated method stub
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String txt = format1.format(arg0);
		return txt;
	}

	@Override
	public Date parse(String arg0, Locale arg1) throws ParseException {
		// TODO Auto-generated method stub
		if(arg0.compareTo("")==0)
			return null;
		Date temp = new SimpleDateFormat("yyyy-MM-dd")
                .parse(arg0);
		return temp;
	}
}