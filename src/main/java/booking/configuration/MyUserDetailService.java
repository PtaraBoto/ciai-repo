package booking.configuration;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import booking.repository.RoleRepository;
import booking.repository.UserRepository;
import booking.Application;
import booking.model.Role;
import booking.model.User;

@Service
class MyUserDetailService implements UserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	private UserRepository users;

	@Autowired
	RoleRepository roles;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = users.findByName(username);

		List<GrantedAuthority> auth = null;
		
		if (user == null) {
			return null;
		}

		Collection<Role> userRoles = user.getRoles();
		Iterator<Role> itRole = userRoles.iterator();
		StringBuilder authorities = new StringBuilder();
		
		while(itRole.hasNext()){
			
			Role currentRole = itRole.next();
			
			if(currentRole.getName().equals("Guest"))
				authorities.append("GUEST,");			
			else if(currentRole.getName().equals("Comment moderator"))
				authorities.append("COMMENT_MODERATOR,");
			else if(currentRole.getName().equals("Hotel manager"))
				authorities.append("HOTEL_MANAGER,");
			else if(currentRole.getName().equals("Web admin"))
				authorities.append("ADMIN,");
		}
		
		authorities.substring(0, authorities.length() - 1);
		auth = AuthorityUtils.commaSeparatedStringToAuthorityList(authorities.toString());
		
		log.debug(user.toString());
		String password = user.getPassword();
		return new org.springframework.security.core.userdetails.User(username, password, auth);
	}
}