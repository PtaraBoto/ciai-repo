package booking.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


	@Autowired
	private MyUserDetailService users;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.anyRequest()
			.permitAll()
			.and()
		.formLogin()
			.loginPage("/login")
			.permitAll()
			.and()
		.logout()
			.permitAll();

//		http
//		.authorizeRequests()
//			.antMatchers("/css/**"
//					, "/js/**"
//					, "/"
//					, "/index"
//					,"/hotels"
//					, "/hotels/**"
//					, "/roomtype/**"
//					, "/users/new"
//					, "/comments/**")
//			.permitAll()
//			.antMatchers("/bookings/**"
//					,"/users/**"
//					,"/comments/form")
//			.hasRole("GUEST")
//			.antMatchers(HttpMethod.PUT.toString(), "/comments/**")
//			.hasRole("GUEST")
//			.antMatchers(HttpMethod.PUT.toString(), "/comments/**")
//			.hasRole("COMMENT_MODERATOR")
//			.antMatchers(HttpMethod.PUT.toString(), "/comments/**")
//			.hasRole("HOTEL_MANAGER")
//			.antMatchers("/hotels/**"
//					,"/roomtype/"
//					,"/rooms/**"
//					,"comments/**")
//			.hasRole("HOTEL_MANAGER")
//			.antMatchers("/**")
//			.hasRole("ADMIN")
//		.and()
//			.formLogin()
//			.loginPage("/login")
//			.permitAll()
//		.and()
//			.logout()
//			.permitAll();

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		PasswordEncoder encoder = new BCryptPasswordEncoder();

		auth.userDetailsService(users).passwordEncoder(encoder);
	}
}