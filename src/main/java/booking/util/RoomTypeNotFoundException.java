package booking.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such RoomType")  // 404
public class RoomTypeNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;
}
