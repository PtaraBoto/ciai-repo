package booking.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import booking.model.Role;
import booking.model.User;
import booking.repository.RoleRepository;
import booking.repository.UserRepository;
import booking.security.IsAdmin;

@Controller
@RequestMapping(value="/users")

public class UserController {

	@Autowired
	UserRepository users;

	@Autowired
	RoleRepository roles;

	private static final long GU = 0;
	private static final long CM = 1;
	private static final long HM = 2;
	private static final long AD = 3;

	private static final String GUS = "GUEST";
	private static final String CMS = "Comment moderator";
	private static final String HMS = "Hotel manager";
	private static final String ADS = "Web admin";

	// GET  /users 			- the list of hotels
	@RequestMapping(method=RequestMethod.GET)
	@IsAdmin
	public String index(Model model) {

		Iterable<User> listUsers = users.findAll();
		Iterable<User> pendingCM = users.findByStatus(1);
		Iterable<User> pendingHM = users.findByStatus(2);
		Iterable<User> pendingAD = users.findByStatus(3);
		model.addAttribute("users", listUsers);

		if(pendingCM.iterator().hasNext())
			model.addAttribute("pendingcm", pendingCM);
		else
			model.addAttribute("pendingcm", null);

		if(pendingHM.iterator().hasNext())
			model.addAttribute("pendinghm", pendingHM);
		else
			model.addAttribute("pendinghm", null);

		if(pendingAD.iterator().hasNext())
			model.addAttribute("pendingad", pendingAD);
		else
			model.addAttribute("pendingad", null);

		return "users/index";
	}

	// GET  /user.json 			- the list of hotels
	@RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
	public @ResponseBody Iterable<User> indexJSON(Model model) {
		return users.findAll();
	}

	@RequestMapping(value="{id}/accept", method=RequestMethod.POST)
	@IsAdmin
	public String accept(@PathVariable("id") long id, Model model) {

		User user = users.findOne(id);
		int currentStatus = user.getStatus();
		Collection<Role> newRoles = user.getRoles();

		switch(currentStatus){
		case 1: 
			newRoles.add(roles.findOne(CM));
			break;
		case 2: 
			newRoles.add(roles.findOne(HM));
			break;
		case 3: 
			newRoles.add(roles.findOne(AD));
			break;
		}

		user.setRoles(newRoles);
		user.setStatus(0);
		users.save(user);
		return "redirect:/users";
	}

	@RequestMapping(value="{id}/reject", method=RequestMethod.POST)
	@IsAdmin
	public String reject(@PathVariable("id") long id, Model model) {

		User user = users.findOne(id);
		user.setStatus(0);
		users.save(user);
		return "redirect:/users";
	}


	@RequestMapping(value="/myprofile", method=RequestMethod.GET) 
	public String show(Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); 
		User currentUser = users.findByName(name);

		Collection<Role> userRoles = currentUser.getRoles();
		Iterator<Role> it = userRoles.iterator();

		model.addAttribute("needcm", true);
		model.addAttribute("needhm", true);
		model.addAttribute("needad", true);

		while(it.hasNext()){
			Role currentRole = it.next();
			if(currentRole.getName().equals(CMS))
				model.addAttribute("needcm", false);
			else if(currentRole.getName().equals(HMS))
				model.addAttribute("needhm", false);
			else if(currentRole.getName().equals(ADS))
				model.addAttribute("needad", false);
		}
		
		if(userRoles.size() == 4)
			model.addAttribute("allroles", true);
		else
			model.addAttribute("allroles", false);
		if(currentUser.getStatus() == 0)
			model.addAttribute("pending", false);
		else
			model.addAttribute("pending", true);
		

		model.addAttribute("user", currentUser);
		return "users/show";
	}

	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String newUser(Model model) {

		model.addAttribute("usertoadd", new User());
		return "users/form";
	}

	@RequestMapping(value="/create", method=RequestMethod.POST)
	public String createUser(User user, Model model) {

		Collection<Role> rolesToAdd = new ArrayList<Role>();
		rolesToAdd.add(roles.findByName("Guest"));
		user.setRoles(rolesToAdd);

		PasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));

		users.save(user);

		return "redirect:/login";
	}	
	
	@RequestMapping(value="/askfor/{id}", method=RequestMethod.POST)
	public String requestOfUser(@PathVariable("id") int id, Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); 
		User currentUser = users.findByName(name);
	
		currentUser.setStatus(id);
		users.save(currentUser);
		return "redirect:/users/myprofile";
	}
}
