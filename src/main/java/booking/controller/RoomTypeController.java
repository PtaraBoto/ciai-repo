package booking.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import booking.model.Hotel;
import booking.model.RoomType;
import booking.modelAux.TimeInterval;
import booking.repository.HotelRepository;
import booking.repository.RoomTypeRepository;
import booking.security.AllowRoomTypeCriationEdition;
import booking.security.AllowedForEditHotel;
import booking.util.RoomTypeNotFoundException;

@Controller
@RequestMapping(value="/roomtype")
public class RoomTypeController {

	@Autowired
    HotelRepository hotels;
	
	@Autowired
	RoomTypeRepository roomtypes;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	// GET  /roomtypes 			- the list of hotels
    @RequestMapping(method=RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("roomtypes", roomtypes.findAll());
        return "roomtype/index";
    }

	// GET  /roomtypes.json 			- the list of hotels
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<RoomType> indexJSON(Model model) {
        return roomtypes.findAll();
    }

    // GET  /roomtypes/new			- the form to fill the data for a new hotel
    @RequestMapping(value="/new/{hotelid}", method=RequestMethod.GET)
    @AllowedForEditHotel
    public String newRoomType(@PathVariable("hotelid") long id, Model model) {
    	Hotel tmpHotel = hotels.findOne(id);
    	model.addAttribute("hotel", tmpHotel);
    	model.addAttribute("roomtype", new RoomType(tmpHotel));
    	return "roomtype/create";
    }
    
    // POST /hotels         	- creates a new hotel
    @RequestMapping(method=RequestMethod.POST)
    @AllowedForEditHotel
    public String saveIt(@ModelAttribute RoomType roomtype, Model model) {
    	roomtypes.save(roomtype);
    	model.addAttribute("hotel", roomtype.getHotel());
    	model.addAttribute("roomtypes", roomtype.getHotel().getRoomTypes()); 
    	return "redirect:/hotels/" + roomtype.getHotel().getId();
    }
    
    
    // GET  /roomtypes/{id} 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET) 
    public String show(@PathVariable("id") long id, Model model) {
    	RoomType roomtype = roomtypes.findOne(id);
    	if( roomtype == null )
    		throw new RoomTypeNotFoundException();
    	
    	model.addAttribute("hotel", roomtype.getHotel());
    	model.addAttribute("roomtype", roomtype);
    	model.addAttribute("rooms", roomtype.getRooms());
    	model.addAttribute("timeinterval", new TimeInterval());
    	return "roomtype/show";
    }
    
    // GET  /hotels/{id}.json 		- the hotel with identifier {id}
//    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
//    public @ResponseBody Hotel showJSON(@PathVariable("id") long id, Model model) {
//    	Hotel hotel = hotels.findOne(id);
//    	if( hotel == null )
//    		throw new HotelNotFoundException();
//    	return hotel;
//    }
    
    // GET  /hotels/{id}/edit 	- the form to edit the hotel with identifier {id}
    @RequestMapping(value="{id}/edit", method=RequestMethod.GET)
    @AllowRoomTypeCriationEdition
    public String edit(@PathVariable("id") long id, Model model) {
    	RoomType tmp = roomtypes.findOne(id);
    	model.addAttribute("roomtype", tmp);
    	model.addAttribute("hotel", tmp.getHotel());
    	return "roomtype/edit";
    }
   
    // POST /hotels/{id} 	 	- update the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.POST)
    @AllowRoomTypeCriationEdition
    public String editSave(@PathVariable("id") long id, RoomType roomtype, Model model) {
    	roomtypes.save(roomtype);
    	return "redirect:/roomtype/" + roomtype.getId();
    }
	
}
