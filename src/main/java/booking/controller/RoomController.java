package booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import booking.model.Hotel;
import booking.model.Room;
import booking.model.RoomType;
import booking.repository.HotelRepository;
import booking.repository.RoomRepository;
import booking.repository.RoomTypeRepository;
import booking.security.AllowRoomTypeCriationEdition;
import booking.security.AllowedForEditHotel;
import booking.security.AllowedRoomCreationEdition;
import booking.util.RoomTypeNotFoundException;

@Controller
@RequestMapping(value="/rooms")
public class RoomController {

	@Autowired
    HotelRepository hotels;
	
	@Autowired
	RoomTypeRepository roomtypes;
	
	@Autowired
	RoomRepository rooms;
	
	// GET  /roomtypes 			- the list of hotels
//    @RequestMapping(method=RequestMethod.GET)
//    public String index(Model model) {
//        model.addAttribute("roomtypes", roomtypes.findAll());
//        return "roomtype/index";
//    }

	// GET  /roomtypes.json 			- the list of hotels
//    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
//    public @ResponseBody Iterable<RoomType> indexJSON(Model model) {
//        return roomtypes.findAll();
//    }

    // GET  /roomtypes/new			- the form to fill the data for a new hotel
    @RequestMapping(value="/new/{roomtypeid}", method=RequestMethod.GET)
    @AllowRoomTypeCriationEdition
    public String newRoom(@PathVariable("roomtypeid") long id, Model model) {
    	
    	RoomType tmproomtype = roomtypes.findOne(id);
   	    	
    	model.addAttribute("hotel", tmproomtype.getHotel());
    	model.addAttribute("roomtype", tmproomtype);
    	model.addAttribute("room", new Room(tmproomtype));
    	
    	return "rooms/create";
    }
    
    // POST /hotels         	- creates a new hotel
    @RequestMapping(method=RequestMethod.POST)
    @AllowRoomTypeCriationEdition
    public String saveIt(@ModelAttribute Room room, Model model) {
    	rooms.save(room);
    	return "redirect:/roomtype/" + room.getRoomtype().getId();
    }
    
    // GET  /roomtypes/{id} 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET) 
    @AllowedRoomCreationEdition
    public String show(@PathVariable("id") long id, Model model) {
    	Room room = rooms.findOne(id);
    	
    	if( room == null )
    		throw new RoomTypeNotFoundException();
    	
    	model.addAttribute("hotel", room.getRoomtype().getHotel());
    	model.addAttribute("roomtype", room.getRoomtype()); 
    	model.addAttribute("room", room);
    	return "rooms/show";
    }
    
    // GET  /hotels/{id}.json 		- the hotel with identifier {id}
//    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
//    public @ResponseBody Hotel showJSON(@PathVariable("id") long id, Model model) {
//    	Hotel hotel = hotels.findOne(id);
//    	if( hotel == null )
//    		throw new HotelNotFoundException();
//    	return hotel;
//    }
    
    // GET  /hotels/{id}/edit 	- the form to edit the hotel with identifier {id}
    @RequestMapping(value="{id}/edit", method=RequestMethod.GET)
    @AllowedRoomCreationEdition
    public String edit(@PathVariable("id") long id, Model model) {
    	Room room = rooms.findOne(id);
    	model.addAttribute("hotel", room.getRoomtype().getHotel());
    	model.addAttribute("roomtype", room.getRoomtype()); 
    	model.addAttribute("room", room);
    	return "rooms/edit";
    }
   
    // POST /hotels/{id} 	 	- update the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.POST)
    @AllowedRoomCreationEdition
    public String editSave(@PathVariable("id") long id, Room room, Model model) {
    	rooms.save(room);
    	System.out.println("--------->"+room.getId());
    	return "redirect:/rooms/" + room.getId();
    }
}
