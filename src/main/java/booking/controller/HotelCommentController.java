package booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import booking.model.Hotel;
import booking.model.HotelComment;
import booking.repository.HotelCommentRepository;
import booking.security.IsCommentModerator;
import booking.security.IsGuest;

@Controller
@RequestMapping(value="/comments")
public class HotelCommentController {

	@Autowired
	HotelCommentRepository hotelcommnets;
	
	// POST /comments/{id} 	 	- update the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.POST)
    @IsGuest
    public String newPost(@PathVariable("id") long id, HotelComment comment, Model model) {
       	hotelcommnets.save(comment);
    	return "redirect:/hotels/" + comment.getHotel().getId();
    }
    
    @RequestMapping(value="{id}/delete", method=RequestMethod.POST)
    @IsCommentModerator
    public String edit(@PathVariable("id") long id, Model model) {
    	
    	Hotel h = hotelcommnets.findOne(id).getHotel();
    	hotelcommnets.delete(id);
    	return "redirect:/hotels/" + h.getId();
    }
    
    @RequestMapping(value="{id}/cofirmPost", method=RequestMethod.POST)
    @IsCommentModerator
    public String acceptComment(@PathVariable("id") long id, Model model) {
    	
    	HotelComment hc = hotelcommnets.findOne(id);
    	hc.setStatus(true);
    	hotelcommnets.save(hc);
    	return "redirect:/hotels/" + hc.getHotel().getId();
    }
}
