package booking.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import booking.model.Booking;
import booking.model.RoomType;
import booking.model.User;
import booking.modelAux.TimeInterval;
import booking.repository.BookingRepository;
import booking.repository.RoomTypeRepository;
import booking.repository.UserRepository;
import booking.security.AllowedForConfRejecBooking;
import booking.security.IsGuest;
import booking.util.RoomTypeNotFoundException;

@Controller
@RequestMapping(value="/bookings")

public class BookingController {

	@Autowired
	RoomTypeRepository roomtypes;
	
	@Autowired
	BookingRepository bookings;

	@Autowired
	UserRepository users;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
    @RequestMapping(value="{id}", method=RequestMethod.GET) 
    @IsGuest
    public String show(@ModelAttribute TimeInterval timeInterval, @PathVariable("id") long id, Model model) {
    	RoomType roomtype = roomtypes.findOne(id);
    	if( roomtype == null )
    		throw new RoomTypeNotFoundException();
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
        User currentUser = users.findByName(name);
        
    	Date start = timeInterval.getStartDate();
    	Date end = timeInterval.getEndDate();
    	
    	long diff = end.getTime() - start.getTime();
    	long days = TimeUnit.MILLISECONDS.toDays(diff); 
    	
    	float price = roomtype.getPrice()*days;
    	
    	int nOfRooms = roomtype.getRooms().size();
    	int nOfOccupiedRooms = 0;
    	Collection<Booking> sameTimeBooking =  bookings.findBySameTimeBooking(start, end, 0, roomtype.getId());
    	nOfOccupiedRooms = sameTimeBooking.size();
    	
    	boolean isAvailable = nOfOccupiedRooms < nOfRooms;

    	Booking booking = new Booking(price, start, end, roomtype, currentUser,1);
    	
    	model.addAttribute("isavailable", isAvailable);
    	model.addAttribute("nofavailablerooms", nOfRooms - nOfOccupiedRooms);
    	model.addAttribute("hotel", roomtype.getHotel());
    	model.addAttribute("roomtype", roomtype);
    	model.addAttribute("booking", booking);
    	
    	return "bookings/available";
    }
    
    // POST /Booking/{id} 	 --- make reservation
    @RequestMapping(value="{id}", method=RequestMethod.POST)
    @IsGuest
    public String makeReservation(@PathVariable("id") long id, Booking booking, Model model) {
    	bookings.save(booking);
    	return "redirect:/users/myprofile";
    }
    
    @RequestMapping(value="{id}/accept", method=RequestMethod.GET)
    @AllowedForConfRejecBooking
    public String accept(@PathVariable("id") long id, Model model) {
        
    	Booking booking = bookings.findOne(id);
    	booking.setAccepted(0);
    	
    	bookings.save(booking);
    	return "redirect:/hotels/" + booking.getBookedRoomType().getHotel().getId();
    }
    
    @RequestMapping(value="{id}/decline", method=RequestMethod.GET)
    @AllowedForConfRejecBooking
    public String decline(@PathVariable("id") long id, Model model) {
        
    	Booking booking = bookings.findOne(id);
    	long hotelId = booking.getBookedRoomType().getHotel().getId();
    	bookings.delete(booking);
       	return "redirect:/hotels/" + hotelId;
    }
}
