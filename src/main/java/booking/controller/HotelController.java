package booking.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import booking.model.Booking;
import booking.model.Hotel;
import booking.model.HotelComment;
import booking.model.RoomType;
import booking.model.User;
import booking.modelAux.OccupancyReport;
import booking.modelAux.TimeInterval;
import booking.repository.BookingRepository;
import booking.repository.HotelCommentRepository;
import booking.repository.HotelRepository;
import booking.repository.UserRepository;
import booking.security.AllowedForEditHotel;
import booking.security.AllowedForHotelCreation;
import booking.util.HotelNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

/*
 * Mapping
 * GET  /hotels 			- the list of hotels
 * GET  /hotels/new			- the form to fill the data for a new hotel 
 * POST /hotels         	- creates a new hotel
 * GET  /hotels/{id} 		- the hotel with identifier {id}
 * GET  /hotels/{id}/edit 	- the form to edit the hotel with identifier {id}
 * POST /hotels/{id} 	 	- update the hotel with identifier {id}
 */

@Controller
@RequestMapping(value="/hotels")
public class HotelController {

    @Autowired
    HotelRepository hotels;

    @Autowired
	UserRepository users;
    
	@Autowired
	BookingRepository bookings;
    
    @Autowired
	HotelCommentRepository hotelcommnets;
    
	// GET  /hotels 			- the list of hotels
    @RequestMapping(method=RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("hotels", hotels.findAll());
        return "hotels/index";
    }

	// GET  /hotels.json 			- the list of hotels
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Hotel> indexJSON(Model model) {
        return hotels.findAll();
    }

    // GET  /hotels/new			- the form to fill the data for a new hotel
    @RequestMapping(value="/new", method=RequestMethod.GET)
    @AllowedForHotelCreation
    public String newHotel(Model model) {
    	model.addAttribute("hotel", new Hotel());
    	return "hotels/create";
    }
    
    // POST /hotels         	- creates a new hotel
    @RequestMapping(method=RequestMethod.POST)
    @AllowedForHotelCreation
    public String saveIt(@ModelAttribute Hotel hotel, Model model) {
    	hotels.save(hotel);
    	model.addAttribute("hotel", hotel);
    	return "redirect:/hotels";
    }
    
    // GET  /hotels/{id} 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET) 
    public String show(@PathVariable("id") long id, Model model) {
    	Hotel hotel = hotels.findOne(id);
    	
    	Collection<HotelComment> aprovedHotelcomments = hotelcommnets.findByStatusTrueAndHotelIdOrderByDate(id);
    	if(aprovedHotelcomments.size() == 0)
    		aprovedHotelcomments = null;
    	
    	Collection<HotelComment> nonAprovedComments = hotelcommnets.findByStatusFalseAndHotelIdOrderByDate(id);
    	if(nonAprovedComments.size() == 0)
    		nonAprovedComments = null;
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
        User currentUser = users.findByName(name);
        
        Collection<Booking> pendigHotelBookings = bookings.findByAcceptedAndBookedRoomTypeHotel(1, hotel);
        if(pendigHotelBookings.size() == 0)
        	pendigHotelBookings = null;
        
    	if( hotel == null )
    		throw new HotelNotFoundException();
    	
    	model.addAttribute("timeinterval", new TimeInterval());
    	model.addAttribute("hotel", hotel );
    	model.addAttribute("roomtypes", hotel.getRoomTypes()); 
    	model.addAttribute("acceptedcomments", aprovedHotelcomments); 
    	model.addAttribute("pendingcomments", nonAprovedComments);
    	model.addAttribute("addcomment", new HotelComment(currentUser, hotel));
    	model.addAttribute("pendingbookings", pendigHotelBookings);
    	
    	return "hotels/show";
    }
    
    // GET  /hotels/{id}.json 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Hotel showJSON(@PathVariable("id") long id, Model model) {
    	Hotel hotel = hotels.findOne(id);
    	if( hotel == null )
    		throw new HotelNotFoundException();
    	return hotel;
    }
    
    // GET  /hotels/{id}/edit 	- the form to edit the hotel with identifier {id}
    @RequestMapping(value="{id}/edit", method=RequestMethod.GET)
    @AllowedForEditHotel
    public String edit(@PathVariable("id") long id, Model model) {
    	model.addAttribute("hotel", hotels.findOne(id));
    	return "hotels/edit";
    }
   
    // POST /hotels/{id} 	 	- update the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.POST)
    @AllowedForEditHotel
    public String editSave(@PathVariable("id") long id, Hotel hotel, Model model) {
    	hotels.save(hotel);
    	return "redirect:/hotels/" + hotel.getId();
    }
    
    @RequestMapping(value="{id}/occupancy", method=RequestMethod.GET)
    @AllowedForEditHotel
    public String occupancy(@PathVariable("id") long id,@ModelAttribute TimeInterval timeInterval, Model model) {
    	
    	Date start = timeInterval.getStartDate();
    	Date end = timeInterval.getEndDate();
    	
    	Hotel hotel = hotels.findOne(id);
    	Collection<OccupancyReport> reportList = new ArrayList<OccupancyReport>();
    	Collection<RoomType> roomTypes = hotel.getRoomTypes();
    	
    	Iterator<RoomType> itrt = roomTypes.iterator();	
    	while(itrt.hasNext()){
    		RoomType currentRoomType = itrt.next();
    		Collection<Booking> bookingsAtRange = bookings.findBySameTimeBooking(start, end, 0, currentRoomType.getId());
    		Collection<Booking> bookingsAtRangePendig = bookings.findBySameTimeBooking(start, end, 1, currentRoomType.getId());
    		OccupancyReport nor = new OccupancyReport(currentRoomType.getName(), bookingsAtRange.size(), 
    				currentRoomType.getRooms().size(), bookingsAtRangePendig.size());
    		reportList.add(nor);
    	}
    	
    	model.addAttribute("reports", reportList);
    	model.addAttribute("timeInterval", timeInterval);
    	model.addAttribute("hotel", hotel);
    	return "hotels/occupancy";
    }
}







