package booking.dataBaseUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import booking.model.Booking;
import booking.model.Hotel;
import booking.model.HotelComment;
import booking.model.Role;
import booking.model.Room;
import booking.model.RoomType;
import booking.model.User;


public class PopulateDB {

	private Collection<User> hotelManagers;
	private ArrayList<User> usersToAddBookings;
	
	public PopulateDB(){
		this.hotelManagers = new ArrayList<User>();
		this.usersToAddBookings = new ArrayList<>();
	}

	public Collection<Hotel> seedsHotel(){

		Collection<Hotel> seedHotel = new ArrayList <Hotel>();
		
		Iterator<User> it = hotelManagers.iterator();
		
		User tmp = it.next();
		seedHotel.add(new Hotel("Pestana","5731 Cleveland Avenue Mount Juliet, TN 37122","Beach hotel",4,tmp));
		seedHotel.add(new Hotel("Triumph","8636 Grant Street Dover, NH 03820","City hotel",2,tmp));
		tmp = it.next();
		seedHotel.add(new Hotel("Fairmount","505 Virginia Street Lake Charles, LA 70605","Culture hotel",3,tmp));
		tmp = it.next();
		seedHotel.add(new Hotel("Radisson","293 Laurel Drive Buffalo Grove, IL 60089","Beach hotel",5,tmp));
		seedHotel.add(new Hotel("Hilton Garden","462 Rose Street Park Forest, IL 60466","Nature hotel",5,tmp));

		return seedHotel;
	}

	public Collection<HotelComment> seedsHotelComments(Collection<Hotel> seedHotel, ArrayList<User> seedUsers){

		Collection<HotelComment> seedcoments = new ArrayList<HotelComment>();
		Iterator<Hotel> itHotel = seedHotel.iterator();
		
		
		Hotel tmp = itHotel.next();
		seedcoments.add(new HotelComment(seedUsers.get(0), "aaaaaaa mmm vbiuabsdf asodifhb afo oabf oi aifbibd aiifbadf aoidikkdf aiobfsabif aiibdf iasbf adbf", new Date(1440000001), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(2), "jha aofiio biadhooi oahfio ihoaf ihsfdih shdfi isjhh sdhifbop", new Date(1423476324), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(1), "skjhgcgoasugfoaohsvou abdoyug oagd 0oa 0bah apbahdvf agduyfh aovfoda aodvfha avf", new Date(1437849174), tmp, false));
		seedcoments.add(new HotelComment(seedUsers.get(5), "kjashva aovdf  oad oad aposf  ohd aohdv aopf  adv afpadv vf", new Date(1265137945), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(7), "jasbhdf apbdvf poad poavdf aodvf aovfbd ha doyvwe ovhf whfyvs ", new Date(1439867545), tmp, true));

		tmp = itHotel.next();
		seedcoments.add(new HotelComment(seedUsers.get(9), "aaaaaaa mmm vbiuabsdf asodifhb afo oabf oi aifbibd aiifbadf aoidikkdf aiobfsabif aiibdf iasbf adbf", new Date(1195708535), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(5), "jha aofiio biadhooi oahfio ihoaf ihsfdih shdfi isjhh sdhifbop", new Date(1023785439), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(4), "skjhgcgoasugfoaohsvou abdoyug oagd 0oa 0bah apbahdvf agduyfh aovfoda aodvfha avf", new Date(1356938579), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(2), "kjashva aovdf  oad oad aposf  ohd aohdv aopf  adv afpadv vf", new Date(1349573628), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(0), "jasbhdf apbdvf poad poavdf aodvf aovfbd ha doyvwe ovhf whfyvs ", new Date(1279374759), tmp, true));
		
		tmp = itHotel.next();
		seedcoments.add(new HotelComment(seedUsers.get(1), "aaaaaaa mmm vbiuabsdf asodifhb afo oabf oi aifbibd aiifbadf aoidikkdf aiobfsabif aiibdf iasbf adbf", new Date(1254697645), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(7), "jha aofiio biadhooi oahfio ihoaf ihsfdih shdfi isjhh sdhifbop", new Date(1198453674), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(9), "skjhgcgoasugfoaohsvou abdoyug oagd 0oa 0bah apbahdvf agduyfh aovfoda aodvfha avf", new Date(1391648521), tmp, true));

		tmp = itHotel.next();
		seedcoments.add(new HotelComment(seedUsers.get(6), "aaaaaaa mmm vbiuabsdf asodifhb afo oabf oi aifbibd aiifbadf aoidikkdf aiobfsabif aiibdf iasbf adbf", new Date(1189343256), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(7), "jha aofiio biadhooi oahfio ihoaf ihsfdih shdfi isjhh sdhifbop", new Date(1198453674), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(5), "vbiuabsdf asodifhb ihsfdih adiuf adpiofh adpfubap apfuwqp hasu asdb aidsvb aid ia diasdhbasdub ", new Date(1361845958), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(7), "asdp uqhge iuab iks osdi qocio q0 pa qej nq eqwje pq w a jfqi ijd qw iq wkqwe qowem qbeiqwn qieh", new Date(1249174901), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(0), "askdnoaosdboiqowejvoaspdqolno pqnwo q ckaj dao qhh ap dnqi ns iq ne asdoj qi aos ao qh qohcaos qo caos", new Date(1356104839), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(3), "skjhgcgoasugfoaohsvou abdoyug oagd 0oa 0bah apbahdvf agduyfh aovfoda aodvfha avf", new Date(1275047910), tmp, true));

		tmp = itHotel.next();
		seedcoments.add(new HotelComment(seedUsers.get(2), "asdas mmeeqwm vbiuaqwbsdf asodifhb afo oabf oi eqwaifbibd aiifbadf aoidikkdf aiobfsabif aiibdf iasbf adbf", new Date(1013847194), tmp, true));
		seedcoments.add(new HotelComment(seedUsers.get(1), "jha aofiio biadqwehooi oahfio qweihoaf ihsfdih shdfqweqi isjhh sdhifbop", new Date(1264913648), tmp, true));

		return seedcoments;
	}

	public Collection<RoomType> seedsRoomType(Collection<Hotel> seedHotel){

		Collection<RoomType> seedRoomTypes = new ArrayList <RoomType>();
		Iterator<Hotel> itHotel = seedHotel.iterator();

		Hotel tmp = itHotel.next();
		seedRoomTypes.add(new RoomType("PentHouse", "description1 blablabla", 200.05f, tmp));
		seedRoomTypes.add(new RoomType("Standart", "description2 blablabla", 90.25f, tmp));

		tmp = itHotel.next();
		seedRoomTypes.add(new RoomType("Standart", "descriptionStandart blablabla", 25.30f, tmp));

		tmp = itHotel.next();
		seedRoomTypes.add(new RoomType("ForTwo", "descriptionTwo blablabla", 45.00f, tmp));
		seedRoomTypes.add(new RoomType("Forone", "descriptionOne blablabla", 25.00f, tmp));

		tmp = itHotel.next();
		seedRoomTypes.add(new RoomType("Suite", "descriptionSuite blablabla", 110.00f, tmp));
		seedRoomTypes.add(new RoomType("Penthouse", "descriptionPenthouse blablabla", 330.00f, tmp));
		seedRoomTypes.add(new RoomType("Standart", "descriptionstandart5 blablabla", 60.00f, tmp));

		tmp = itHotel.next();
		seedRoomTypes.add(new RoomType("Suite", "descriptionSuite blablabla", 200.00f, tmp));

		return seedRoomTypes;
	}

	public Collection<Room> seedRooms(Collection<RoomType> seedRoomTypes){

		Collection<Room> seedRooms = new ArrayList <Room>();
		Iterator<RoomType> itRoomTypes = seedRoomTypes.iterator();
		
		RoomType tmp = itRoomTypes.next();
		seedRooms.add(new Room(2, "001", tmp));
		tmp = itRoomTypes.next();
		seedRooms.add(new Room(1, "002", tmp));
		seedRooms.add(new Room(1, "003", tmp));

		tmp = itRoomTypes.next();
		seedRooms.add(new Room(1, "001", tmp));
		seedRooms.add(new Room(1, "002", tmp));
		seedRooms.add(new Room(2, "003", tmp));
		seedRooms.add(new Room(2, "004", tmp));

		tmp = itRoomTypes.next();
		seedRooms.add(new Room(1, "01", tmp));
		seedRooms.add(new Room(1, "02", tmp));
		tmp = itRoomTypes.next();
		seedRooms.add(new Room(1, "03", tmp));
		seedRooms.add(new Room(2, "04", tmp));

		tmp = itRoomTypes.next();
		seedRooms.add(new Room(2, "01", tmp));
		seedRooms.add(new Room(2, "02", tmp));
		tmp = itRoomTypes.next();
		seedRooms.add(new Room(3, "03", tmp));
		tmp = itRoomTypes.next();
		seedRooms.add(new Room(3, "04", tmp));
		seedRooms.add(new Room(3, "05", tmp));
		seedRooms.add(new Room(3, "06", tmp));
		seedRooms.add(new Room(3, "07", tmp));
		seedRooms.add(new Room(3, "08", tmp));

		tmp = itRoomTypes.next();
		seedRooms.add(new Room(0, "01-E", tmp));
		seedRooms.add(new Room(0, "02-W", tmp));
		seedRooms.add(new Room(0, "03-N", tmp));
		seedRooms.add(new Room(0, "04-S", tmp));
		seedRooms.add(new Room(0, "05-C", tmp));

		return seedRooms;
	}

	public ArrayList<Role> seedRoles(){

		ArrayList<Role> seedRoles = new ArrayList <Role>();

		seedRoles.add(new Role(0,"Guest"));
		seedRoles.add(new Role(1,"Comment moderator"));
		seedRoles.add(new Role(2,"Hotel manager"));
		seedRoles.add(new Role(3,"Web admin"));

		return seedRoles;
	}

	public ArrayList<User> seedUsers(ArrayList<Role> roles){

		ArrayList<User> seedUsers = new ArrayList <User>();
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		
		ArrayList<Role> rolesToAdd1 = new ArrayList<Role>();
		rolesToAdd1.add(roles.get(0));		
		
		User x = new User("Jon",encoder.encode("pass"), rolesToAdd1, 1);
		seedUsers.add(x);
		usersToAddBookings.add(x);
		x = new User("Mary",encoder.encode("pass"), rolesToAdd1, 0);
		seedUsers.add(x);
		usersToAddBookings.add(x);
		x = new User("Rupert",encoder.encode("pass"), rolesToAdd1, 0);
		seedUsers.add(x);
		usersToAddBookings.add(x);
		x = new User("Jarred",encoder.encode("pass"), rolesToAdd1, 0);
		seedUsers.add(x);
		usersToAddBookings.add(x);


		//add Comment Moderators
		ArrayList<Role> rolesToAdd2 = new ArrayList<Role>();
		rolesToAdd2.add(roles.get(0));
		rolesToAdd2.add(roles.get(1));
		seedUsers.add(new User("Maike",encoder.encode("pass"), rolesToAdd2, 0));
		seedUsers.add(new User("Radha",encoder.encode("pass"), rolesToAdd2, 0));
		seedUsers.add(new User("Dulce",encoder.encode("pass"), rolesToAdd2, 2));

		//add Hotel Managers
		ArrayList<Role> rolesToAdd3 = new ArrayList<Role>();
		rolesToAdd3.add(roles.get(0));
		rolesToAdd3.add(roles.get(2));
		
		User tmp = new User("Corinne",encoder.encode("pass"), rolesToAdd3, 1);
		seedUsers.add(tmp);
		hotelManagers.add(tmp);
		
		tmp = new User("Morwenna",encoder.encode("pass"), rolesToAdd3, 0);
		seedUsers.add(tmp);
		hotelManagers.add(tmp);
		
		ArrayList<Role> rolesToAdd4 = new ArrayList<Role>();
		rolesToAdd4.add(roles.get(0));
		rolesToAdd4.add(roles.get(2));
		rolesToAdd4.add(roles.get(1));
		tmp = new User("Seti",encoder.encode("pass"), rolesToAdd4, 0);
		seedUsers.add(tmp);
		hotelManagers.add(tmp);
		
		//add Admin
		ArrayList<Role> rolesToAdd5 = new ArrayList<Role>();
		rolesToAdd5 = new ArrayList<Role>();
		rolesToAdd5.add(roles.get(3));
		seedUsers.add(new User("Boto Admin",encoder.encode("pass"), rolesToAdd5, 0));
		
		return seedUsers;
	}
	
	public Collection<Booking> seedBookings(Collection<RoomType> roomTypes){

		Collection<Booking> seedBookings = new ArrayList <Booking>();

		Iterator<RoomType> itR = roomTypes.iterator();
		
		RoomType rt = itR.next();
		
		//first roomType
		User u = usersToAddBookings.get(0);
		seedBookings.add(new Booking(159, new Date(115, 11, 14), new Date(115, 11, 19), rt, u, 0));
		seedBookings.add(new Booking(220, new Date(116, 1, 1), new Date(116, 1, 4), rt, u, 0));
		u = usersToAddBookings.get(1);
		seedBookings.add(new Booking(113, new Date(116, 1, 5), new Date(116, 1, 8), rt, u, 0));
		
		rt = itR.next();
		//second roomType
		seedBookings.add(new Booking(113, new Date(116, 1, 2), new Date(116, 1, 5), rt, u, 0));
		u = usersToAddBookings.get(2);
		seedBookings.add(new Booking(113, new Date(116, 1, 3), new Date(116, 1, 6), rt, u, 0));
		u = usersToAddBookings.get(0);
		seedBookings.add(new Booking(113, new Date(116, 2, 17), new Date(116, 2, 23), rt, u, 0));
		
		rt = itR.next();
		//third roomType
		seedBookings.add(new Booking(113, new Date(116, 1, 1), new Date(116, 1, 5), rt, u, 1));
		u = usersToAddBookings.get(3);
		seedBookings.add(new Booking(113, new Date(116, 1, 2), new Date(116, 1, 5), rt, u, 0));
		u = usersToAddBookings.get(2);
		seedBookings.add(new Booking(113, new Date(116, 1, 3), new Date(116, 1, 5), rt, u, 0));
		u = usersToAddBookings.get(1);
		seedBookings.add(new Booking(113, new Date(116, 1, 4), new Date(116, 1, 5), rt, u, 0));

		return seedBookings;
	}
}
