package booking.security;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import booking.model.Hotel;
import booking.model.Role;
import booking.repository.BookingRepository;
import booking.repository.HotelRepository;
import booking.repository.RoleRepository;
import booking.repository.RoomRepository;
import booking.repository.RoomTypeRepository;
import booking.repository.UserRepository;

@Component("mySecurityService")
public class MySecurityService {

	@Autowired
	HotelRepository hotels;
	
	@Autowired
	RoomTypeRepository roomtypes;

	@Autowired
	UserRepository users;

	@Autowired
	RoleRepository roles;
	
	@Autowired
	RoomRepository rooms;
	
	@Autowired
	BookingRepository bookings;

	private static final String GU = "GUEST";
	private static final String CM = "COMMENT_MODERATOR";
	private static final String HM = "HOTEL_MANAGER";
	private static final String AD = "ADMIN";

	public boolean canEditHotel(User user, long hotelId) {

		Hotel hotel = hotels.findOne(hotelId);
		System.out.println("...................>" + hotel.getManager().getName());
		System.out.println("...................>" + user.getUsername());

		return hotel != null && 
				hotel.getManager() != null && 
				((user.getUsername().equals(hotel.getManager().getName()) || 
				authorityListContains(user.getAuthorities(), AD))); 
	}

	public boolean canCreateHotel(User user){

		boolean canDoIt = false;

		System.out.println("-------------+++++++----" + user.getUsername());

		Iterator<GrantedAuthority> itRoles = user.getAuthorities().iterator();

		while(itRoles.hasNext()){	
			GrantedAuthority currentAut = itRoles.next();
			System.out.println(currentAut.getAuthority());
			if(currentAut.getAuthority().equals(HM) || currentAut.getAuthority().equals(AD)){
				System.out.println("true");
				canDoIt = true;
			}
		}
		return canDoIt;
	}
	
	public boolean canAlterRoomType(User user, long roomTypeId){
		Hotel hotel = roomtypes.findOne(roomTypeId).getHotel();
		System.out.println("--------------->11111111111 " + hotel.getManager().getName());
		return canEditHotel(user, hotel.getId());
	}

	public boolean canAlterRoom(User user, long roomId){
		Hotel hotel = rooms.findOne(roomId).getRoomtype().getHotel();
		
		System.out.println("--------------->" + hotel.getManager().getName());
		
		return canEditHotel(user, hotel.getId());
	}
	
	public boolean isGuest(User user){
		return authorityListContains(user.getAuthorities(), GU);
	}
	
	public boolean isCommentModerator(User user){
		return authorityListContains(user.getAuthorities(), CM);
	}
	
	public boolean isAdmin(User user){
		return authorityListContains(user.getAuthorities(), AD);
	}
	
	public boolean canCheckBookings(User user, long bookingId){
		Hotel hotel = bookings.findOne(bookingId).getBookedRoomType().getHotel();
		
		return hotel != null && 
				hotel.getManager() != null && 
				user.getUsername().equals(hotel.getManager().getName());
	}
	
	private boolean authorityListContains(Collection<GrantedAuthority> auth, String role){

		Iterator<GrantedAuthority> it = auth.iterator();

		while(it.hasNext()){
			GrantedAuthority currentAut = it.next();
			if(currentAut.getAuthority().equals(role)){
				return true;
			}
		}
		return false;
	}
}


