package booking.modelAux;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OccupancyReport {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String roomtypename;
	private int nofoccupiedrooms;
	private int nofrooms;
	private int nofpendingininrange;

	public OccupancyReport(){
		
	}
	
	public OccupancyReport(String roomtypename, int nofoccupiedrooms, int nofrooms, int nofpendingininrange){
	
		this.roomtypename = roomtypename;
		this.nofoccupiedrooms = nofoccupiedrooms;
		this.nofrooms = nofrooms;
		this.nofpendingininrange = nofpendingininrange;
	}

	public String getRoomtypename() {
		return roomtypename;
	}

	public void setRoomtypename(String roomtypename) {
		this.roomtypename = roomtypename;
	}

	public int getNofoccupiedrooms() {
		return nofoccupiedrooms;
	}

	public void setNofoccupiedrooms(int nofoccupiedrooms) {
		this.nofoccupiedrooms = nofoccupiedrooms;
	}

	public int getNofrooms() {
		return nofrooms;
	}

	public void setNofrooms(int nofrooms) {
		this.nofrooms = nofrooms;
	}

	public int getNofpendingininrange() {
		return nofpendingininrange;
	}

	public void setNofpendingininrange(int nofpendingininrange) {
		this.nofpendingininrange = nofpendingininrange;
	}
	
}
