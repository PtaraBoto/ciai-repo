package booking.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import booking.model.Role;
import booking.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByName(String name);
	
//	Collection<User> findByRole(Role role);
	
	Collection<User> findByStatus(int status);
}