package booking.repository;

import org.springframework.data.repository.CrudRepository;

import booking.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	Role findByName (String name);
}
