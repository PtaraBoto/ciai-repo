package booking.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import booking.model.Booking;
import booking.model.Hotel;

public interface BookingRepository extends CrudRepository<Booking, Long>{
	
//	Collection<Booking> findByStartDateBetweenOrEndDateBetweenAndAccepted(Date init, Date end, Date init2, Date end2, int accepted);

	 @Query("SELECT b FROM Booking b INNER JOIN b.bookedRoomType rt "
	 		+ "WHERE b.accepted = :acc AND "
	 		+ "rt.id = :rtid AND "
	 		+ "((b.startDate <= :sd AND b.endDate >= :sd) OR"
	 		+ "(b.startDate <= :ed AND b.endDate >= :ed) OR"
	 		+ "(b.startDate >= :sd AND b.endDate <= :ed))")
	Collection<Booking> findBySameTimeBooking(@Param("sd")Date s, @Param("ed")Date e, @Param("acc")int acc, @Param("rtid")long rtid);

	Collection<Booking> findByAcceptedAndBookedRoomTypeHotel(int acc, Hotel hotel);
}
