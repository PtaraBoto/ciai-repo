package booking.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import booking.model.HotelComment;

public interface HotelCommentRepository extends CrudRepository<HotelComment, Long> {

	Collection<HotelComment> findByHotelIdOrderByDate (long hotelid);
	
	Collection<HotelComment> findByStatusTrueAndHotelIdOrderByDate (long hotelid);
	
	Collection<HotelComment> findByStatusFalseAndHotelIdOrderByDate (long hotelid);
}
